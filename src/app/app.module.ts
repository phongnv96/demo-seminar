import { BlogModule } from './pages/blogs/blog.module';
import { MaterialFeatureModule } from './material-feature/material-feature.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './pages/home/home.module';
import { ComponentsModule } from './pages/components/components.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule} from '@angular/common/http';
import { DemoDiComponent } from './demo-di/demo-di.component';
import { PostService } from './services/post.service';
@NgModule({
  declarations: [
    AppComponent,
    DemoDiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialFeatureModule,
    HomeModule,
    ComponentsModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
