import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoDiComponent } from './demo-di.component';

describe('DemoDiComponent', () => {
  let component: DemoDiComponent;
  let fixture: ComponentFixture<DemoDiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoDiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoDiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
