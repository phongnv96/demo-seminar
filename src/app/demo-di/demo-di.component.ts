import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-demo-di',
  templateUrl: './demo-di.component.html',
  styleUrls: ['./demo-di.component.scss'],
  providers: [PostService]
})
export class DemoDiComponent implements OnInit {

  constructor( private ps: PostService){

  }

  ngOnInit(): void {
  }

}
