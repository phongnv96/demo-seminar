import { title } from 'process';
import { HookOnchangeComponent } from './../lifecycle-hook/hook-onchange/hook-onchange.component';
import { LogService } from './../lifecycle-hook/log.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Blog } from 'src/app/models/Blog';
import { ShareDataService } from '../interaction/share-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-component-template',
  templateUrl: './component-template.component.html',
  styleUrls: ['./component-template.component.scss'],
  providers:  [ LogService ]
})
export class ComponentTemplateComponent implements OnInit, OnDestroy {
  constructor(log: LogService, private shareData: ShareDataService) {
    this.logger = log;
    this.hookLog = log.logs;
    this.reset();

    // share data
    const setTitle = this.shareData.getTitle$.subscribe(res => {
      this.title = res;
    });
    this.subcription.add(setTitle);
  }

  subcription = new Subscription();
  title = 'COMPONENTS & TEMPLATES';

  // databinding start
  inputField = '';
  inputColor = '';
  isChangeColor: boolean;
  isDisabledBtn: boolean;
  currentStyles = {};
  // databinding end

  // lifecycle hook start
  hasChild = false;
  hookLog: string[];
  heroName = 'Windstorm';
  private logger: LogService;

  blog: Blog;
  power: string;
  titleHook = 'OnChanges';
  @ViewChild(HookOnchangeComponent) childView: HookOnchangeComponent;

  // lifecycle hook end

  ngOnInit(): void {}

  // databinding fun start
  sum(a, b) {
    return a + b;
  }

  changeColor() {
    this.isChangeColor = !this.isChangeColor;
    this.currentStyles = {
      'background-color':  this.inputColor,
    };
  }

  changeTextColor(event) {
    this.isChangeColor = true;
    this.inputColor = event;
    this.currentStyles = {
      'background-color': event,
    };
  }

  disabledBtnChange() {
    this.isDisabledBtn = !this.isDisabledBtn;
  }
  // databinding fun end

  // lifecycle hook fn start
  toggleChild() {
    this.hasChild = !this.hasChild;
    if (this.hasChild) {
      this.heroName = 'Windstorm';
      this.logger.clear(); // clear log on create
    }
    this.hookLog = this.logger.logs;
    this.logger.tick();
  }

  updateHero() {
    this.heroName += '!';
    this.logger.tick();
  }

  reset() {
    // new Blog object every time; triggers onChanges
    this.blog = {title: 'Blog'};
    // setting power only triggers onChanges if this value is different
    this.power = 'sing';
    if (this.childView) { this.childView.reset(); }
  }
  // lifecycle hook fn end
  ngOnDestroy(): void {
    this.subcription.unsubscribe();
  }
}
