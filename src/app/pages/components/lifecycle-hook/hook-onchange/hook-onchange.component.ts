import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Blog } from 'src/app/models/Blog';

@Component({
  selector: 'app-hook-onchange',
  templateUrl: './hook-onchange.component.html',
  styleUrls: ['./hook-onchange.component.scss']
})
export class HookOnchangeComponent implements OnInit, OnChanges {

  @Input() blog: Blog;
  @Input() power: string;

  changeLog: string[] = [];
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    for (const propName in changes) {
      const chng = changes[propName];
      const cur  = JSON.stringify(chng.currentValue);
      const prev = JSON.stringify(chng.previousValue);
      this.changeLog.push(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
    }
  }

  reset() { this.changeLog = []; }
}
