import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {

  constructor() { }
  title = 'I am parent';
  ngOnInit(): void {
  }
  changeTitle(event) {
    this.title = event;
  }
}
