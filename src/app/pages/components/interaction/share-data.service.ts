import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShareDataService {
  subject = new Subject();
  constructor() { }

  get getTitle$(): Observable<any> {
    return this.subject.asObservable();
  }

  setTitle(title: any) {
    this.subject.next(title);
  }

}
