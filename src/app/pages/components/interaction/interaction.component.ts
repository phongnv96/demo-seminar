import { ShareDataService } from './share-data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.scss']
})
export class InteractionComponent implements OnInit {

  constructor(private shareData: ShareDataService) { }
  title = '';
  ngOnInit(): void {
  }

  changePageTitle() {
    this.shareData.setTitle(this.title);
  }

}
