import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentTemplateComponent } from './component-template/component-template.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialFeatureModule } from 'src/app/material-feature/material-feature.module';
import { FormsModule } from '@angular/forms';
import { LifecycleHookComponent } from './lifecycle-hook/lifecycle-hook.component';
import { HookOnchangeComponent } from './lifecycle-hook/hook-onchange/hook-onchange.component';
import { InteractionComponent } from './interaction/interaction.component';
import { ParentComponent } from './interaction/parent/parent.component';
import { ChildComponent } from './interaction/child/child.component';



@NgModule({
  declarations: [
    ComponentTemplateComponent,
    LifecycleHookComponent,
    HookOnchangeComponent, InteractionComponent,
    ParentComponent, ChildComponent],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    MaterialFeatureModule
  ],
  exports: [ComponentTemplateComponent]
})
export class ComponentsModule { }
