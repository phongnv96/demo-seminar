import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.container.html',
  styleUrls: ['./blog.container.scss']
})
// tslint:disable-next-line:component-class-suffix
export class BlogContainer implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
