import { BlogRoutingModule } from './blog-routing.module';
import { MaterialFeatureModule } from '../../material-feature/material-feature.module';
import { CommonModule } from '@angular/common';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogCardComponent } from './blog-card/blog-card.component';
import { NgModule } from '@angular/core';
import { BlogContainer } from './blog-container/blog.container';
import { BlogAddComponent } from './blog-add/blog-add.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        BlogCardComponent,
        BlogListComponent,
        BlogDetailComponent,
        BlogContainer,
        BlogAddComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MaterialFeatureModule,
        BlogRoutingModule
    ],
    exports: [
        BlogCardComponent,
        BlogListComponent,
        BlogDetailComponent,
        BlogContainer,
        BlogAddComponent
    ],
    providers: [],
})
export class BlogModule { }
