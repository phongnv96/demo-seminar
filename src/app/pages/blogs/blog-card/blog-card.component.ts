import { BlogAddComponent } from '../blog-add/blog-add.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Blog } from 'src/app/models/Blog';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-blog-card',
  templateUrl: './blog-card.component.html',
  styleUrls: ['./blog-card.component.scss']
})
export class BlogCardComponent implements OnInit {
  @Input() blogInfor: Blog;
  @Output() getNameById = new EventEmitter();
  constructor( private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  gotoDetail(id) {
    this.router.navigate([`/blog/detail/${id}`]);
  }

  openEditDialog(): void {
    const dialogRef = this.dialog.open(BlogAddComponent, {
      width: '800px',
      data: {name: '', animal: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getNameBlogById(id){
    this.getNameById.emit(id);
  }

}
