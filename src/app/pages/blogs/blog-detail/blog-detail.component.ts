import { BlogService } from '../../../services/blog.service';
import { Observable } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { Blog } from 'src/app/models/Blog';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private blogService: BlogService) { }
  // @Input() blogPost: Blog;
  blog$: Observable<Blog>;

  ngOnInit(): void {
    this.blog$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.blogService.getBlogById(params.get('id')))
    );
  }

}
