import { Blog } from './../../../models/blog';
import { BlogService } from './../../../services/blog.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, ValidatorFn, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-blog-add',
  templateUrl: './blog-add.component.html',
  styleUrls: ['./blog-add.component.scss']
})
export class BlogAddComponent implements OnInit {
  formGroup: FormGroup;
  titleAlert = 'This field is required';
  post: any = '';

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<BlogAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private blogService: BlogService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required, this.alphabetValidator()]],
      subTitle: [null, Validators.required],
      description: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
      more: this.formBuilder.array([
        this.formBuilder.control('')
      ])
    });
  }

  get more() {
    return this.formGroup.get('more') as FormArray;
  }

  addMore() {
    this.more.push(this.formBuilder.control(''));
  }

  get name() {
    return this.formGroup.get('name') as FormControl;
  }


  getErrorName() {
    if (this.formGroup.get('name').hasError('errorAlphabet')) {
      return 'Name only containts characters in aphabet';
    }else if (this.formGroup.get('name').hasError('required')) {
      return 'Name is required ';
    }else {
      return '';
    }
  }

  public alphabetValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const regex = new RegExp('^[A-Za-z ]+$');
      return regex.test(control.value) || control.value === '' ? null : {
        errorAlphabet: true,
      };
    };
  }

  onSubmit(post) {
    console.log(post);
    let blog: Blog = {};
    blog.title = post.name;
    blog.subTitle = post.subTitle;
    blog.description = post.description;
    blog.imageUrl = 'https://i.pinimg.com/originals/55/b9/0f/55b90f7384816fa01276d917061a91b4.jpg';
    this.blogService.addNewBlog(blog).subscribe(res => {
      console.log(res, 'res');
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
