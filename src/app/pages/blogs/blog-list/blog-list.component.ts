import { BlogService } from '../../../services/blog.service';
import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/models/Blog';
import { MatDialog } from '@angular/material/dialog';
import { BlogAddComponent } from '../blog-add/blog-add.component';
import { from } from 'rxjs';
import { filter, map } from 'rxjs/operators';
@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {

  constructor(private blogService: BlogService, public dialog: MatDialog) { }
  blogList: Blog[];
  source = from([1, 2, 3, 4, 5]);
  example = this.source.pipe(filter(num => num % 2 === 0), map( v => v * 10));
  subscribe = this.example.subscribe(val => console.log(`Even number: ${val}`));
  ngOnInit(): void {
    this.loadBlogs();
  }

  loadBlogs() {
    this.blogService.getAllBlog().subscribe(res => {
      this.blogList = res;
    });
  }

  getNameById(id) {
    this.blogService.getNameById(id).subscribe( res => {
      console.log(res, 'NameById');
    });
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(BlogAddComponent, {
      width: '800px',
      data: {name: '', animal: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.loadBlogs();
    });
  }

}


