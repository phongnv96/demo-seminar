import { Blog } from './../models/blog';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BLOGS } from '../models/mock-blogs';
import { map, filter } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  configUrl= "http://localhost:3000/blogs";
  constructor(private http: HttpClient) { }

  getBlogs(): Observable<Blog[]> {
    return of(BLOGS);
  }

  getBlogById(id: any): Observable<Blog> {
    return this.getBlogs().pipe(
      map((blogs: Blog[]) => blogs.find(blog => blog.id === id))
    );
  }

  getAllBlog(): Observable<Blog[]>{
    return this.http.get<Blog[]>(this.configUrl);
  }

  addNewBlog(blog: Blog) {
    return this.http.post(this.configUrl, blog);
  }

  getNameById(id: string){
    return this.http.get(this.configUrl).pipe(
      map((blogs: Blog[]) => blogs.find(blog => blog.id === id)),
    );
  }

}
