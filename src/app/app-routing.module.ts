import { HomePageComponent } from './pages/home/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentTemplateComponent } from './pages/components/component-template/component-template.component';



const routes: Routes = [
  {
    path: 'blog',
    loadChildren: () => import('./pages/blogs/blog.module').then(m => m.BlogModule)
  },
  {
    path: 'components', component: ComponentTemplateComponent,
  },
   
  { path: '', redirectTo: '/home', pathMatch: 'full'  },
  { path: '**', component: HomePageComponent, pathMatch: 'full' },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
