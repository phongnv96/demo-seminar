import { Blog } from './Blog';
import { title } from 'process';

export const BLOGS: Blog[] = [
    {
        id: '1',
        title: 'Shiba Inu',
        imageUrl: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
        subTitle: 'Dog Breed',
        description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.',
        content: 'bcf'
    },
    {
        id: '2',
        title: 'Girl Beautiful',
        imageUrl: 'assets/images/girl.jpg',
        subTitle: 'Lovely girl',
        description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.',
        content: 'bcf'
    },
    {
        id: '3',
        title: 'Girl sexy',
        imageUrl: 'assets/images/girl-2.jpg',
        subTitle: 'Dog Breed',
        description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.',
        content: 'bcf'
    }
];

