export interface Blog {
    id?: string;
    title?: string;
    subTitle?: string;
    imageUrl?: string;
    description?: string;
    content?: string;
}
