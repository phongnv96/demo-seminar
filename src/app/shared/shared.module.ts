import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightCardComponent } from './highlight-card/highlight-card.component';
import { HighlightDirective } from './directives/highlight.directive';
import { ExponentialStrengthPipe } from './pipes/exponential-strength.pipe';



@NgModule({
  declarations: [HighlightCardComponent, HighlightDirective, ExponentialStrengthPipe],
  imports: [
    CommonModule
  ],
  exports: [HighlightCardComponent, HighlightDirective, ExponentialStrengthPipe]
})
export class SharedModule { }
